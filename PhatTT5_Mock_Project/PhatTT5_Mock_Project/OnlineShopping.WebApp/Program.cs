using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using OnlineShopping.Data.Data;
using OnlineShopping.Data.DbInitializer;
using OnlineShopping.Data.Repositories;
using OnlineShopping.Data.Repositories.IRepository;
using OnlineShopping.Utility;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.

builder.Services.AddControllersWithViews();
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseSqlServer(connectionString));

//builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
//    .AddEntityFrameworkStores<ApplicationDbContext>();

builder.Services.AddDatabaseDeveloperPageExceptionFilter();

//builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
//    .AddEntityFrameworkStores<ApplicationDbContext>();
builder.Services.AddIdentity<IdentityUser, IdentityRole>().AddDefaultTokenProviders()
    .AddEntityFrameworkStores<ApplicationDbContext>();
builder.Services.AddSingleton<IEmailSender, EmailSender>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<IDbInitializer, DbInitializer>();

builder.Services.AddRazorPages();
builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = $"/Identity/Account/Login";
    options.LogoutPath = $"/Identity/Account/Logout";
});
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(100);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});
var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

SeedDatabase();
app.UseAuthentication();

app.UseAuthorization();
app.UseSession();
app.MapRazorPages();
app.MapControllerRoute(
    name: "default",
    pattern: "{area=Customer}/{controller=Product}/{action=Index}/{id?}");
app.MapControllerRoute(
    name: "login",
    pattern: "login",
    defaults: new { area = "Login", controller = "Login", action = "Index" });
app.MapControllerRoute(
    name: "register",
    pattern: "register",
    defaults: new { area = "Login", controller = "Login", action = "Register" });
app.MapControllerRoute(
    name: "category",
    pattern: "category",
    defaults: new { area = "Customer", controller = "Category", action = "Index" });
app.MapControllerRoute(
    name: "product",
    pattern: "product",
    defaults: new { area = "Customer", controller = "Product", action = "Index" });
app.MapControllerRoute(
    name: "cart",
    pattern: "cart",
    defaults: new { area = "Customer", controller = "Cart", action = "Index" });
app.MapControllerRoute(
    name: "admin",
    pattern: "{area=Admin}/{controller=Home}/{action=Index}/{id?}");
app.MapControllerRoute(
    name: "admin/products",
    pattern: "admin/products",
    defaults: new { area = "Admin", controller = "Product", action = "Index" });
app.MapControllerRoute(
    name: "admin/categories",
    pattern: "admin/categories",
    defaults: new { area = "Admin", controller = "Category", action = "Index" });
app.MapControllerRoute(
    name: "add_product",
     pattern: "{area=Admin}/{controller=Product}/{action=AddEdit}/{id?}");

app.Run();


void SeedDatabase()
{
    using (var scope = app.Services.CreateScope())
    {
        var dbInitializer = scope.ServiceProvider.GetRequiredService<IDbInitializer>();
        dbInitializer.Initialize();
    }
}
