﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineShopping.Data.Repositories.IRepository;
using OnlineShopping.Model;
using OnlineShopping.Model.ViewModel;

namespace OnlineShopping.WebApp.Areas.Admin
{
    [Area("Admin")]
    [Authorize(Roles = "Admin,Employee")]
    public class CategoryController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvironment;

        public CategoryController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _hostEnvironment = hostEnvironment;
        }

        public IActionResult Index(SearchFilterViewModel searchFilterViewModel)
        { 
            IEnumerable<Category> categories = _unitOfWork.Category.GetAll();
            if (searchFilterViewModel.KeyWord != null)
            {
                categories = categories.Where(_ => _.Name.Contains(searchFilterViewModel.KeyWord)).ToList();
                ViewBag.KeyWord = searchFilterViewModel.KeyWord;
            }
            int count = categories.Count();
            int pageSize = searchFilterViewModel.PageSize > 9 ? searchFilterViewModel.PageSize : 9;
            int pageNumber = searchFilterViewModel.PageNumber > 1 ? searchFilterViewModel.PageNumber : 1;
            categories = categories.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0) + 1;
            ViewBag.Page = pageNumber;
            return View(categories);
        }

        //GET
        public IActionResult AddEdit(int? id)
        {
            Category category = new();
            if (id == null || id == 0)
            {
                return View(category);
            }
            else
            {
                category = _unitOfWork.Category.GetFirstOrDefault(_ => _.Id == id);
                return View(category);
            }
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddEdit(Category obj)
        {
            if (ModelState.IsValid)
            {
                string wwwRootPath = _hostEnvironment.WebRootPath;
               
                if (obj.Id == 0)
                {
                    _unitOfWork.Category.Add(obj);
                }
                else
                {
                    _unitOfWork.Category.Update(obj);
                }
                _unitOfWork.Save();
                TempData["success"] = "Caterogy created successfully";
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var category = _unitOfWork.Category.GetFirstOrDefault(u => u.Id == id);

            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteItem(int? id)
        {
            var obj = _unitOfWork.Category.GetFirstOrDefault(_ => _.Id == id);
            var listProduct = _unitOfWork.Product.GetAll(_ => _.CategoryId == id);
            foreach(var item in listProduct)
            {
                _unitOfWork.Product.Remove(item);
            }
            if (obj == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            _unitOfWork.Category.Remove(obj);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        public IActionResult Detail(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var category = _unitOfWork.Category.GetFirstOrDefault(u => u.Id == id);

            if (category == null)
            {
                return NotFound();
            }

            return View(category);
        }
    }
}
