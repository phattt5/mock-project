﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace OnlineShopping.WebApp.Areas.Admin
{
    [Area("Admin")]
    [Authorize(Roles = "Admin,Employee")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
