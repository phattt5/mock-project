﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using OnlineShopping.Data.Repositories.IRepository;
using OnlineShopping.Model;
using OnlineShopping.Model.ViewModel;

namespace OnlineShopping.WebApp.Areas.Admin
{
    [Area("Admin")]
    [Authorize(Roles = "Admin,Employee")]
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _hostEnvironment;

        public ProductController(IUnitOfWork unitOfWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _hostEnvironment = hostEnvironment;
        }

        public IActionResult Index(SearchFilterViewModel searchFilterViewModel)
        {
            ViewData["Category"] = _unitOfWork.Category.GetAll();
            IEnumerable<Product> objProductList = _unitOfWork.Product.GetAll();
            if (searchFilterViewModel.CategoryId != null)
            {
                objProductList = objProductList.Where(_ => _.CategoryId == searchFilterViewModel.CategoryId).ToList();
                ViewBag.CategoryId = searchFilterViewModel.CategoryId;
            }
            if (searchFilterViewModel.KeyWord != null)
            {
                objProductList = objProductList.Where(_ => _.Name.Contains(searchFilterViewModel.KeyWord)).ToList();
                ViewBag.KeyWord = searchFilterViewModel.KeyWord;
            }
            int count = objProductList.Count();
            int pageSize = searchFilterViewModel.PageSize > 9 ? searchFilterViewModel.PageSize : 9;
            int pageNumber = searchFilterViewModel.PageNumber > 1 ? searchFilterViewModel.PageNumber : 1;
            objProductList = objProductList.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0) + 1;
            ViewBag.Page = pageNumber;
            return View(objProductList);
        }

        //GET
        public IActionResult AddEdit(int? id)
        {
            ProductViewModel productVM = new()
            {
                Product = new(),
                CategoryList = _unitOfWork.Category.GetAll().Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                }),
            };

            if (id == null || id == 0)
            {
                return View(productVM);
            }
            else
            {
                productVM.Product = _unitOfWork.Product.GetFirstOrDefault(_ => _.Id == id);
                return View(productVM);
            }
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddEdit(ProductViewModel obj, IFormFile? file)
        {
            if (ModelState.IsValid)
            {
                string wwwRootPath = _hostEnvironment.WebRootPath;
                if (file != null)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var uploads = Path.Combine(wwwRootPath, @"images\products");
                    var extension = Path.GetExtension(file.FileName);

                    if (obj.Product.ImageUrl != null)
                    {
                        var oldImagePath = Path.Combine(wwwRootPath, obj.Product.ImageUrl.TrimStart('\\'));
                        if (System.IO.File.Exists(oldImagePath))
                        {
                            System.IO.File.Delete(oldImagePath);
                        }
                    }

                    using (var fileStreams = new FileStream(Path.Combine(uploads, fileName + extension), FileMode.Create))
                    {
                        file.CopyTo(fileStreams);
                    }
                    obj.Product.ImageUrl = @"\images\products\" + fileName + extension;
                }
                if (obj.Product.Id == 0)
                {
                    _unitOfWork.Product.Add(obj.Product);
                }
                else
                {
                    _unitOfWork.Product.Update(obj.Product);
                }
                _unitOfWork.Save();
                TempData["success"] = "Product created successfully";
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var product = _unitOfWork.Product.GetFirstOrDefault(u => u.Id == id, includeProperties: "Category");

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteItem(int? id)
        {
            var obj = _unitOfWork.Product.GetFirstOrDefault(_ => _.Id == id);
            if (obj == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            var oldImagePath = Path.Combine(_hostEnvironment.WebRootPath, obj.ImageUrl.TrimStart('\\'));
            if (System.IO.File.Exists(oldImagePath))
            {
                System.IO.File.Delete(oldImagePath);
            }
            _unitOfWork.Product.Remove(obj);
            _unitOfWork.Save();
            return RedirectToAction("Index");
        }

        public IActionResult Detail(int id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var product = _unitOfWork.Product.GetFirstOrDefault(u => u.Id == id, includeProperties: "Category");

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }
    }
}