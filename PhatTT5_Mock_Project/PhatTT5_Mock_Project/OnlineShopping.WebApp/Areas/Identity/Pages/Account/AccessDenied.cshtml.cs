﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace OnlineShopping.WebApp.Areas.Identity.Pages.Account
{
    public class AccessDeniedModel : PageModel
    {
        public async Task<IActionResult> OnGet(string? returnUrl = null)
        {
            return Page();
        }
    }
}

