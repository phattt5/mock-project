﻿using Microsoft.AspNetCore.Mvc;

namespace OnlineShopping.Web.Areas.Customer
{
    [Area("Customer")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
