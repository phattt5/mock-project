﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OnlineShopping.Data.Repositories.IRepository;
using OnlineShopping.Model;
using OnlineShopping.Model.ViewModel;
using System.Security.Claims;

namespace OnlineShopping.Web.Areas.Customer
{
    [Area("Customer")]
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IActionResult Index(SearchFilterViewModel searchFilterViewModel)
        {
            ViewData["Category"] = _unitOfWork.Category.GetAll();
            IEnumerable<Product> objProductList = _unitOfWork.Product.GetAll();
            if (searchFilterViewModel.CategoryId != null)
            {
                objProductList = objProductList.Where(_ => _.CategoryId == searchFilterViewModel.CategoryId).ToList();
                ViewBag.CategoryId = searchFilterViewModel.CategoryId;
            }
            if (searchFilterViewModel.KeyWord != null)
            {
                objProductList = objProductList.Where(_ => _.Name.Contains(searchFilterViewModel.KeyWord)).ToList();
                ViewBag.KeyWord = searchFilterViewModel.KeyWord;
            }
            int count = objProductList.Count();
            int pageSize = searchFilterViewModel.PageSize > 9 ? searchFilterViewModel.PageSize : 9;
            int pageNumber=searchFilterViewModel.PageNumber>1?searchFilterViewModel.PageNumber:1;
            objProductList = objProductList.Skip((pageNumber-1) * pageSize).Take(pageSize).ToList();

            ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0)+1;

            ViewBag.Page = pageNumber;
            return View(objProductList);
        }
        public IActionResult Detail(int id)
        {
            Cart cartObj = new()
            {
                Count = 1,
                ProductId = id,
                Product = _unitOfWork.Product.GetFirstOrDefault(u => u.Id == id, includeProperties: "Category"),
            };

            return View(cartObj);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public IActionResult Detail(Cart shoppingCart)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var claim = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier);
            shoppingCart.ApplicationUserId = claim.Value;
            Cart cartFromDb = _unitOfWork.Cart.GetFirstOrDefault(
            u => u.ApplicationUserId == claim.Value && u.ProductId == shoppingCart.ProductId);


            if (cartFromDb == null)
            {

                _unitOfWork.Cart.Add(new Cart
                {
                    ProductId=shoppingCart.ProductId,
                    ApplicationUserId = claim.Value,
                    Count=shoppingCart.Count
                });
                _unitOfWork.Save();
                HttpContext.Session.SetInt32("SessionShoppingCart",
                    _unitOfWork.Cart.GetAll(u => u.ApplicationUserId == claim.Value).ToList().Count);
            }
            else
            {
                _unitOfWork.Cart.IncrementCount(cartFromDb, shoppingCart.Count);
                _unitOfWork.Save();
            }


            return RedirectToAction(nameof(Index));
        }
    }
}
