﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using OnlineShopping.Data.Data;
using OnlineShopping.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShopping.Data.DbInitializer
{
    public class DbInitializer : IDbInitializer
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _db;

        public DbInitializer(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            ApplicationDbContext db)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _db = db;
        }


        public void Initialize()
        {
            //migrations if they are not applied
            try
            {
                if (_db.Database.GetPendingMigrations().Count() > 0)
                {
                    _db.Database.Migrate();
                }
            }
            catch (Exception ex)
            {

            }

            ////create roles if they are not created
            if (!_roleManager.RoleExistsAsync("Admin").GetAwaiter().GetResult())
            {
                AddUser();
            }
            if (!_db.Categories.Any())
            {
                AddCategory();
            }
            if (!_db.Products.Any())
            {
                AddProduct();
            }

            return;
        }
        private void AddUser()
        {
            _roleManager.CreateAsync(new IdentityRole("Admin")).GetAwaiter().GetResult();
            _roleManager.CreateAsync(new IdentityRole("Employee")).GetAwaiter().GetResult();
            _roleManager.CreateAsync(new IdentityRole("Customer")).GetAwaiter().GetResult();

            //if roles are not created, then we will create admin user as well

            _userManager.CreateAsync(new ApplicationUser
            {
                UserName = "PhatTT5@gmail.com",
                Email = "PhatTT5@gmail.com",
                Name = "Truong Tai Phat",
            }, "Admin@123").GetAwaiter().GetResult();

            ApplicationUser user = _db.ApplicationUsers.FirstOrDefault(u => u.Email == "PhatTT5@gmail.com");

            _userManager.AddToRoleAsync(user, "Admin").GetAwaiter().GetResult();




            _userManager.CreateAsync(new ApplicationUser
            {
                UserName = "PhatTT6@gmail.com",
                Email = "PhatTT6@gmail.com",
                Name = "Employee",
            }, "Employee@123").GetAwaiter().GetResult();

            ApplicationUser userEmployee = _db.ApplicationUsers.FirstOrDefault(u => u.Email == "PhatTT6@gmail.com");

            _userManager.AddToRoleAsync(userEmployee, "Employee").GetAwaiter().GetResult();

            _userManager.CreateAsync(new ApplicationUser
            {
                UserName = "PhatTT7@gmail.com",
                Email = "PhatTT7@gmail.com",
                Name = "Customer",
            }, "Customer@123").GetAwaiter().GetResult();

            ApplicationUser userCustomer = _db.ApplicationUsers.FirstOrDefault(u => u.Email == "PhatTT7@gmail.com");

            _userManager.AddToRoleAsync(userCustomer, "Customer").GetAwaiter().GetResult();

        }
        private void AddCategory()
        {
            _db.Categories.AddRange(
                  new Category {Name = "Fruits And Vegetables", CreatedDateTime = DateTime.UtcNow },
                  new Category {Name = "PersonalCare", CreatedDateTime =  DateTime.UtcNow },
                  new Category {Name = "Grocery & Staples", CreatedDateTime =  DateTime.UtcNow }
                );
            _db.SaveChanges();
        }
        private void AddProduct()
        {
            _db.Products.AddRange(
                  new Product { Name = "Sampann-Toor-Dals", Price =50 , DiscountPrice=45, Description="", ImageUrl="/images/bv2.png", CategoryId=1 },
                  new Product { Name = "Parryss-Sugar", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv3.png", CategoryId = 1 },
                  new Product { Name = "Saffola-Gold", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv4.png", CategoryId = 1 },
                  new Product { Name = "Sampann-Toor-Dal", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv5.png", CategoryId = 1 },
                  new Product { Name = "Parryss-Sugar", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv6.png", CategoryId = 1 },
                  new Product { Name = "Sampann-Toor-Dals", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv7.png", CategoryId = 1 },
                  new Product { Name = "Sampann-Toor-Dals", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv2.png", CategoryId = 1 },
                  new Product { Name = "Sampann-Toor-Dals", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv3.png", CategoryId = 1 },
                  new Product { Name = "Sampann-Toor-Dals", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv4.png", CategoryId = 1 },
                  new Product { Name = "Sampann-Toor-Dals", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv5.png", CategoryId = 1 },
                  new Product { Name = "Sampann-Toor-Dals", Price = 50, DiscountPrice = 45, Description = "", ImageUrl = "/images/bv6.png", CategoryId = 1 }
                );
            _db.SaveChanges();
        }
    }
}
