﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShopping.Data.DbInitializer
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
