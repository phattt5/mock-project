﻿using OnlineShopping.Model;

namespace OnlineShopping.Data.Repositories.IRepository
{
    public interface ICartRepository : IRepository<Cart>
    {
        int IncrementCount(Cart shoppingCart, int count);

        int DecrementCount(Cart shoppingCart, int count);
    }
}