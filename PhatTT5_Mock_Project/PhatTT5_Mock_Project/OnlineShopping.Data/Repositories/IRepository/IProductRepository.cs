﻿using OnlineShopping.Model;

namespace OnlineShopping.Data.Repositories.IRepository
{
    public interface IProductRepository : IRepository<Product>
    {
        void Update(Product obj);
    }
}