﻿using OnlineShopping.Data.Data;
using OnlineShopping.Data.Repositories.IRepository;
using OnlineShopping.Model;

namespace OnlineShopping.Data.Repositories
{
    public class CartRepository : Repository<Cart>, ICartRepository
    {
        private ApplicationDbContext _db;

        public CartRepository(ApplicationDbContext db) : base(db)
        {
           _db = db;
        }

        public int DecrementCount(Cart shoppingCart, int count)
        {
            shoppingCart.Count -= count;
            return shoppingCart.Count;
        }

        public int IncrementCount(Cart shoppingCart, int count)
        {
            shoppingCart.Count += count;
            return shoppingCart.Count;
        }
    }
}